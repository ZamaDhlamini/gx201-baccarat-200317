﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardScript : MonoBehaviour
{
    //this script will be a storage place for info on each card
    
    //standard value of cards
    public int value = 0;

    public int GetValueOfCards()
    {
        return value;
    }

    public void SetValue(int newValue)
    {
        // Taking in a value as a new argument
        value = newValue;
    }
    public void SetSprite(Sprite newSprite)
    {
        // Taking in a value as a new argument
        gameObject.GetComponent<SpriteRenderer>().sprite = newSprite;
    }

    public string GetSpriteName()
    {
        //this will be for cards only visible on the table and not the whole deck
        return GetComponent<SpriteRenderer>().sprite.name;
    }

    public void ResetCards()
    {
        Sprite back = GameObject.Find("DECK").GetComponent<DeckScript>().GetCardsBack();
        // flipping the card over and making its card value back at 0
        gameObject.GetComponent<SpriteRenderer>().sprite = back;
        value = 0;
    }
}
