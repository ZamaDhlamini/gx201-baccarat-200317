﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserScript : MonoBehaviour
{
    // This script is for the player and the dealer
    // keeping track of the dealers cards and deal them out

    public CardScript cardScript;
    public DeckScript deckScript;

    //This is the total value of the dealers hand
    public int handValue = 0;
    // how much money the user is allowed to bet on
    private int money = 100;
    // array to hold the card objects on the table
    public GameObject[] hand;
    //index of the next card when turned over
    public int cardIndex = 0;
    

    public void StartHand()
    {
        ReceiveCard();
        ReceiveCard();
    }

    //getting cards to the player on the board
    public int ReceiveCard()
    {
        //get a card
        int cardValue = deckScript.DealCards(hand[cardIndex].GetComponent<CardScript>());
        // shows cards on screen
        hand[cardIndex].GetComponent<Renderer>().enabled = true;
        //adds up card value to hand total
        handValue += cardValue;
        return handValue;
    }




// adding or subtracting money from our cash amount for betting
    public void AdjustMoney(int amount){
       money += amount;
    }
    
    //Outputing players current amount
    public int GetMoney(){
        return money;
    }
    
}
