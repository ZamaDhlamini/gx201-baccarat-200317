﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    //accesses to our buttons
    public Button dealButton;
    public Button betButton;
    public Button drawButton;
    
    // have access to the userscript which has the hand
    public UserScript playerScript;
    public UserScript bankerScript;
    
    // have access to our HUD text
    public Text playerScoreText;
    public Text bankerScoreText;
    public Text betText;
    
    //public Text mainText;
    public Text cashText;

    //gameobject hiding the banker and the players third card
    public GameObject hideCard;

    //how much is being bet on
    int betMoney = 0;

    void Start()
    {
        // Add a on click listeners to the buttons
        dealButton.onClick.AddListener(() => DealClicked());
        betButton.onClick.AddListener(() => BetClicked());
        drawButton.onClick.AddListener(() => DrawClicked());
    }

    private void DealClicked(){
        // Not hidden banker score, betting is done before dealing cards
        bankerScoreText.gameObject.SetActive(true);
        GameObject.Find("DECK").GetComponent<DeckScript>().Shuffle();
       playerScript.StartHand();
       bankerScript.StartHand();
       //updating the players and banker score
       playerScoreText.text = "P HAND:" + playerScript.handValue.ToString();
       bankerScoreText.text = "B HAND:" + bankerScript.handValue.ToString();
       // Display buttons when needed
       dealButton.gameObject.SetActive(false);
       //we only want them to be active when after the hand
       drawButton.gameObject.SetActive(true);
       betMoney = 250;
    }

    private void DrawClicked(){
        //if the player or banker hand has less than 9 give the player and banker another card
        if (playerScript.ReceiveCard() <= 9)
        {
            playerScript.ReceiveCard();
        }
    }
    private void BetClicked(){

    }

    

   
}
