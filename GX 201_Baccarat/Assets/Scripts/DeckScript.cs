﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckScript : MonoBehaviour
{
    public Sprite[] cardSprites;
    int[] cardValues = new int[53];
    int currentIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
       GetCardValues(); 
    }

    // Values of the cards
    void GetCardValues()
    {
        int num = 0;
        // I am looping so that values are assigned to cards
        for (var i = 0; i < cardSprites.Length; i++)
        {
            num = i;
            num %= 13;
            if (num > 9 || num == 0)
            {
                num = 9;
                Debug.Log("assinging card values");
            }
            cardValues [i] = num++;
        }
        currentIndex = 1;
        
    }
// this is where the game shuffles the cards randomly
// This function will also be using an array data swapping
    public void Shuffle()
    {
        for (int i = cardSprites.Length -1; i > 0; --i)
        {
            // this will give a random number within the deck of cards
          int j = Mathf.FloorToInt(Random.Range(0.0f, 1.0f) * cardSprites.Length - 1) + 1;
          //assigning cardsprites to the face variable
          Sprite face = cardSprites[i];
          //replacing cardsprites i with j
          cardSprites[i] = cardSprites[j];
          //take the face value that was i and put j in place
          cardSprites[j] = face;
          //doing exactly the same with these values
          int value = cardValues[i];
          cardValues[i] = cardValues[j];
          cardValues[j] = value;  
        }
    }

    public int DealCards(CardScript cardScript)
    {
        // deal out cards and also set the cardsprites and deal out the current index

        cardScript.SetSprite(cardSprites[currentIndex]);
        //set the value of sprite 
        cardScript.SetValue(cardValues[currentIndex]);
        //incrementing by 1 only gets added after we set the value
        currentIndex++;
        return cardScript.GetValueOfCards();
       
    }

    public Sprite GetCardsBack()
    {
        //allows deck script to control the deck and the cards simultaneously
        return cardSprites[0];
    }
}
