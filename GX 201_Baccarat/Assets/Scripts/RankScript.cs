﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RankScript : MonoBehaviour
{
    private int ranks;
    
    [System.Serializable]
    public struct CardFaces
    {
        public CardSuits suits;
        public CardValues values;
    public CardFaces(CardSuits suits, CardValues values){
        this.suits = suits;
        this.values = values;
    }
    }


    public enum CardSuits
    {
        Diamonds,
        Hearts,
        Clubs,
        Spades,
    }
  public enum CardValues
  {
      Ace = 1, 
      Two = 2,
      Three = 3,
      Four = 4,
      Five = 5,
      Six = 6,
      Seven = 7,
      Eight = 8,
      Nine = 9,
      Ten = 0,
      Jack = 0,
      Queen = 0,
      King = 0,

  }
  
 
  
}
